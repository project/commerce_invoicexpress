<?php

namespace Drupal\commerce_invoicexpress\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure commerce_invoicexpress settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_invoicexpress_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_invoicexpress.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_invoicexpress.settings');
    $form['account_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ACCOUNT_NAME'),
      '#default_value' => $config->get('account_name'),
      '#description' => $this->t("Enter the account name."),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API_KEY'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t("Enter the API key."),
      '#required' => TRUE,
    ];

    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#default_value' => $config->get('email_subject'),
      '#description' => $this->t("Enter the subject for the email sent when an invoice is generated ."),
      '#required' => TRUE,
    ];

    $form['email_body'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email body'),
      '#default_value' => $config->get('email_body'),
      '#description' => $this->t("Enter the body for the email sent when an invoice is generated ."),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_invoicexpress.settings')
      ->set('account_name', $form_state->getValue('account_name'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('email_subject', $form_state->getValue('email_subject'))
      ->set('email_body', $form_state->getValue('email_body'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
