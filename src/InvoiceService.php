<?php

namespace Drupal\commerce_invoicexpress;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Calculator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class InvoiceService.
 *
 * @package Drupa\commerce_invoicexpress
 */
class InvoiceService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * LoggerChannelInterface object.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * InvoiceService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   commerce_invoicexpress settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $loggerFactory, LanguageManagerInterface $languageManager) {
    $this->configFactory = $configFactory->get('commerce_invoicexpress.settings');
    $this->logger = $loggerFactory->get('commerce_invoicexpress');
    $this->languageManager = $languageManager;
  }


  /**
   * prepare Client data for InvoiceXpress API.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @return array
   *   Client data.
   */
  public function prepareClient(Order $order) {
    $customer_profiles = $order->getBillingProfile()
      ->get('address')
      ->getValue();
    $customer_profile = reset($customer_profiles);

    $country_list = \Drupal::getContainer()->get('country_manager')->getList();
    $country_name = $country_list[$customer_profile['country_code']]->render();

    $client = [
      'name' => $customer_profile['given_name'] . ' ' . $customer_profile['family_name'],
      'email' => $order->getEmail(),
      'address' => $customer_profile['address_line1'],
      'city' => $customer_profile['locality'],
      'postal_code' => $customer_profile['postal_code'],
      'country' => $country_name,
      //DC from Drupal Commerce to prevent overrides
      'code' => 'DC-'. $order->getCustomerId(),
      'fiscal_id' => $customer_profile['vat_number']
    ];

    return $client;
  }

  /**
   * Compute Invoice due date from config.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @return mixed
   *   Due date in seconds.
   */
  public function computeDueDate(Order $order) {
    $due_date = $order->getPlacedTime() + (3600 * 24 * $this->configFactory->get('maturity'));

    return $due_date;
  }

  /**
   * Set Invoice data for InvoiceXpress API.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @return array
   *   Invoice data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Being thrown if invalid entity type is provided.
   */
  public function addInvoice(Order $order) {
    $paymentStorage = \Drupal::entityTypeManager()
      ->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\Payment[] $payments */
    $payments = $paymentStorage->loadByProperties([
      'order_id' => $order->id(),
    ]);
    $alreadyPaid = FALSE;
    foreach ($payments as $payment) {
      if ($payment->getState()->value === 'completed') {
        $alreadyPaid = TRUE;
        break;
      }
    }

    $invoice = [
      'name' => $this->configFactory->get('invoice_name_prefix') . $order->getOrderNumber(),
      'variable' => sprintf("%'.08d", $order->getOrderNumber()),
      'constant' => $this->configFactory->get('constant'),
      'specific' => $this->configFactory->get('specific'),
      'already_paid' => $alreadyPaid,
      'invoice_currency' => $order->getTotalPrice()->getCurrencyCode(),
      'invoice_no_formatted' => '',
      'created' => date('Y-m-d', $order->getPlacedTime()),
      'due' => date('Y-m-d', $this->computeDueDate($order)),
      'comment' => '',
      'type' => 'regular',
    ];

    return $invoice;
  }

  /**
   * Prepare Order Item data for InvoiceXpress API.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $item
   *   Order item.
   * @param int $orderDiscount
   *   Percentage value of total Order Discount.
   * @param string $order_discount_label
   *   Label for order discount.
   *
   * @return array
   *   Order Item data.
   *
   * @todo Set 'unit' in order_item dynamically based on item type
   */
  public function prepareOrderItem(OrderItemInterface $item, $orderDiscount = 0, $order_discount_label = NULL) {
    $unit_price = 0;
    $tax_rate = 0;
    $itemDiscount = 0;
    $item_price = Calculator::round($item->getUnitPrice()
      ->getNumber(), 4, PHP_ROUND_HALF_UP);

    $item_adjustments = $item->getAdjustments();
    foreach ($item_adjustments as $item_adjustment) {
      if ($item_adjustment->getType() == 'tax') {
        $tax_percentage = $item_adjustment->getPercentage();
        $tax_rate = Calculator::multiply($tax_percentage, 100);
      }
      elseif ($item_adjustment->getType() == 'promotion') {
        // Add shipping discount percentage to shipping item.
        if (!is_null($item_adjustment->getPercentage())) {
          $itemDiscount = Calculator::multiply($item_adjustment->getPercentage(), 100);
          $order_item_discount_description = $item_adjustment->getLabel();
        }
        elseif (is_null($item_adjustment->getPercentage()) && !is_null($item_adjustment->getAmount()
            ->getNumber())) {
          $item_discount_amount = Calculator::round($item_adjustment->getAmount()
            ->getNumber(), 4, PHP_ROUND_HALF_UP);
          $order_item_discount_description = $item_adjustment->getLabel();
          if (strcmp(abs($item_price), abs($item_discount_amount)) === 0) {
            $itemDiscount = 100;
          }
          else {
            $item_discount_percentage = Calculator::divide($item_discount_amount, $item_price, 4);
            $itemDiscount = abs(Calculator::multiply($item_discount_percentage, 100));
          }
        }
      }
    }

    $totalDiscount = 0;
    // Calculate percentage of total discount.
    if ($orderDiscount != 0) {
      $totalDiscount = 100 - ((1 - ($itemDiscount / 100)) * (1 - ($orderDiscount / 100)) * 100);
      $order_item_discount_description = $order_discount_label;
    }
    else {
      $totalDiscount = $itemDiscount;
    }

    // If there are no tax adjustments, then we would end up without price.
    if ($tax_rate === 0) {
      $unit_price = $item->getUnitPrice()->getNumber();
    }
    else {
      // Calculate and round tax amount (ta)
      // and unit price without tax from order item.
      $unit_price = $item->getUnitPrice()->getNumber();
      $ta_divisor = Calculator::add(100, $tax_rate);
      $ta_divide = Calculator::divide($unit_price, $ta_divisor);
      $ta_multiply = Calculator::multiply($ta_divide, $tax_rate);
      $tax_amount = Calculator::round($ta_multiply, 4, PHP_ROUND_HALF_UP);

      $unit_price = Calculator::subtract($unit_price, $tax_amount, 4);
    }

    $order_item = [
      'name' => $item->getTitle(),
      'description' => '',
      'quantity' => $item->getQuantity(),
      'unit' => 'unit',
      'unit_price' => $unit_price,
      'tax' => $tax_rate,
      'discount' => !empty($totalDiscount) ? $totalDiscount : 0
    ];

    return $order_item;
  }

  /**
   * Set Shipping data for commerce_invoicexpress API.
   *
   * @param \Drupal\commerce_order\Adjustment $shipping_adjustment
   *   Order shipping adjustment.
   * @param array $shipping_data
   *   Shipping data.
   *
   * @return array
   *   Shipping item data.
   */
  public function addShippingItem(Adjustment $shipping_adjustment, array $shipping_data) {
    $shipping = $shipping_adjustment->getAmount()->getNumber();

    // Calculate shipping tax amount and tax rate.
    $tax_rate = Calculator::multiply($shipping_data['shipping_tax_percentage'], 100);
    $shipping_tax_amount = Calculator::divide($shipping, (100 + $tax_rate)) * $tax_rate;

    $shipping_unit_price = Calculator::subtract($shipping, (string) $shipping_tax_amount, 4);

    $shipping_item = [
      'name' => $shipping_adjustment->getLabel(),
      'description' => '',
      'quantity' => 1,
      'unit' => 'unit',
      'unit_price' => $shipping_unit_price,
      'tax' => $tax_rate,
    ];

    // Add shipping discount percentage to shipping item.
    if (!is_null($shipping_data['shipping_discount_percentage'])) {
      $shipping_discount_percentage = Calculator::multiply($shipping_data['shipping_discount_percentage'], 100);
      $shipping_item['discount'] = $shipping_discount_percentage;
      $shipping_item['discount_description'] = $shipping_data['shipping_discount_label'];
    }
    elseif (is_null($shipping_data['shipping_discount_percentage']) && !is_null($shipping_data['shipping_discount_amount'])) {
      $shipping_price = Calculator::round($shipping, 4, PHP_ROUND_HALF_UP);
      $shipping_discount_amount = Calculator::round($shipping_data['shipping_discount_amount'], 4, PHP_ROUND_HALF_UP);
      $shipping_item['discount_description'] = $shipping_data['shipping_discount_label'];
      if (strcmp(abs($shipping_price), abs($shipping_discount_amount)) === 0) {
        $shipping_item['discount'] = 100;
      }
      else {
        $shipping_discount_percentage = Calculator::divide($shipping_discount_amount, $shipping_price, 4);
        $shipping_item['discount'] = abs(Calculator::multiply($shipping_discount_percentage, 100));
      }
    }

    return $shipping_item;
  }

  /**
   * Create Invoice using InvoiceXpress API.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Being thrown if invalid entity type is provided.
   */
  public function createInvoice(Order $order) {
    $order_discount = 0;
    $order_discount_label = NULL;

    // Define shipping tax amount and percentage,
    // shipping promotions amount and percentage default values
    // as shipping_data.
    $shipping_data = [
      'shipping_discount_label' => '',
      'shipping_discount_amount' => NULL,
      'shipping_discount_percentage' => NULL,
      'shipping_tax_amount' => '0',
      'shipping_tax_percentage' => '0',
    ];

    $order_adjustments = $order->getAdjustments();
    /** @var \Drupal\commerce_order\Adjustment $order_adjustment */
    foreach ($order_adjustments as $order_adjustment) {
      if ($order_adjustment->getType() == 'shipping') {
        $shipping_adjustment = $order_adjustment;
      }
      elseif ($order_adjustment->getType() == 'tax') {
        // Get shipping tax amount and percentage from order adjustment.
        $shipping_data['shipping_tax_amount'] = $order_adjustment->getAmount()
          ->getNumber();
        $shipping_data['shipping_tax_percentage'] = $order_adjustment->getPercentage();
      }
      elseif ($order_adjustment->getType() == 'shipping_promotion') {
        $shipping_data['shipping_discount_label'] = $order_adjustment->getLabel();
        $shipping_data['shipping_discount_amount'] = $order_adjustment->getAmount()->getNumber();
        if (!is_null($order_adjustment->getPercentage())) {
          $shipping_data['shipping_discount_percentage'] = $order_adjustment->getPercentage();
        }
      }
      elseif ($order_adjustment->getType() == 'promotion') {
        //$api->setInvoice('discount_total', 0);
        //$api->setInvoice('discount', 0);
        if (!is_null($order_adjustment->getPercentage())) {
          $order_discount = $order_adjustment->getPercentage() * 100;
          $order_discount_label = $order_adjustment->getLabel();
        }
        else {
          //$api->setInvoice('discount_total', $order_adjustment->getAmount()->getNumber());
        }
      }
    }

    if (isset($shipping_adjustment)) {
      //TODO
    }

    $order_date = \Drupal::service('date.formatter')->format($order->getCompletedTime(),'custom', 'd/m/Y');
    $order_due_date = \Drupal::service('date.formatter')->format($order->getCompletedTime(),'custom', 'd/m/Y');
    //$reference = $order->getOrderNumber();
    $client = $this->prepareClient($order);

    $order_items = $order->getItems();
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    foreach ($order_items as $order_item){
      $items[] = $this->prepareOrderItem($order_item);
    }

    $invoice = [
      'date' => $order_date,
      'due_date' => $order_date,
      'due_date' => $order_date,
      //'reference' => $reference,
      'client' => $client,
      'items' => $items,
    ];



    //call the endpoint
    $response = $this->createDocumentEndpoint('invoice', $invoice);
    $invoice_id = $response["invoice"]["id"];

    //Change the status to finalized
    if($invoice_id){
      $response = $this->changeDocumentStateEndpoint('invoice', $invoice_id,'finalized');
      $send_email = $this->sendEmailEndpoint('invoice', $invoice_id, $client['email']);
    }
  }

  /**
   * Call the InvoiceXpress API endpoint.
   *
   * @param string $document_type
   *
   * The type of invoice you want to get. For example:
   * invoices, invoice_receipts, simplified_invoices, vat_moss_invoices, credit_notes or debit_notes.
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function createDocumentEndpoint($document_type, $invoice) {

    $uri = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com' . '/' . $document_type . 's' . '.json?api_key=' . $this->configFactory->get('api_key');

    //Creating a httpClient Object.
    $client = \Drupal::httpClient();

    //Add the type od documento to the array
    $json_data = [$document_type => $invoice];
    $json_data = json_encode($json_data, JSON_UNESCAPED_SLASHES);

    $request = $client->post($uri, [
      'body' => $json_data,
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
      'http_errors' => FALSE,
    ]);
    //Getting Response after JSON Decode.
    return $response = json_decode($request->getBody(), true);
  }

  /**
   * Call the InvoiceXpress API endpoint.
   *
   * @param string $document_type
   *
   * @param int $document_id
   *
   * @param string $document_state
   *
   * The type of invoice you want to get. For example:
   * invoices, invoice_receipts, simplified_invoices, vat_moss_invoices, credit_notes or debit_notes.
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function changeDocumentStateEndpoint(string $document_type, int $document_id, string $document_state) {

    $uri = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com' . '/' . $document_type . '/' . $document_id .'/change-state.json?api_key=' . $this->configFactory->get('api_key');

    //Creating a httpClient Object.
    $client = \Drupal::httpClient();

    //Add the type od documento to the array
    $json_data = [$document_type => ['state' => $document_state]];
    $json_data = json_encode($json_data);

    $request = $client->post($uri, [
      'body' => $json_data,
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
      'http_errors' => FALSE,
    ]);
    //Getting Response after JSON Decode.
    return $response = json_decode($request->getBody(), true);
  }

  /**
   * Call the InvoiceXpress API endpoint.
   *
   * @param string $document_type
   *
   * @param int $document_id
   *
   * @param string $document_state
   *
   * The type of invoice you want to get. For example:
   * invoices, invoice_receipts, simplified_invoices, vat_moss_invoices, credit_notes or debit_notes.
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function sendEmailEndpoint(string $document_type, int $document_id, $email) {

    $uri = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com' . '/' . $document_type . 's'.  '/' . $document_id .'/email-document.json?api_key=' . $this->configFactory->get('api_key');

    //Creating a httpClient Object.
    $client = \Drupal::httpClient();

    //Add the type od documento to the array
    $json_data = [
      "message" => [
        "client" => ['email' => $email, 'save' => '0'],
        "subject" => $this->configFactory->get('email_subject'),
        "body" => $this->configFactory->get('email_body')
      ],
    ];


    $json_data = json_encode($json_data);

    $request = $client->post($uri, [
      'body' => $json_data,
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
      'http_errors' => FALSE,
    ]);
    //Getting Response after JSON Decode.
    return $response = json_decode($request->getBody(), true);
  }

}
