<?php

namespace Drupal\commerce_invoicexpress\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * InvoiceXpress event subscriber.
 */
class InvoiceSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.validate.post_transition' => ['createInvoice', -200],
    ];
  }

  /**
   * Create Invoice in InvoiceXpress from created order.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Transition Event.
   */
  public function createInvoice(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $event->getEntity();
    $invoice = \Drupal::service('commerce_invoicexpress.invoice_service');
    $invoice->createInvoice($order);
  }

}
